// Cynhyrchwyd y ffeil hon yn awtomatig. PEIDIWCH Â MODIWL
// This file is automatically generated. DO NOT EDIT
import {as_connections} from '../models';

export function CreateConnection(arg1:as_connections.ASConnection):Promise<as_connections.ASConnection>;

export function DeleteConnection(arg1:number):Promise<void>;

export function GetAll():Promise<Array<as_connections.ASConnection>>;

export function GetByName(arg1:string):Promise<as_connections.ASConnection>;

export function GetCurrentConnection():Promise<as_connections.ASConnection>;

export function UpdateConnection(arg1:number,arg2:as_connections.ASConnection):Promise<as_connections.ASConnection>;
