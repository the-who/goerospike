export namespace as_connections {
	
	export class ASConnection {
	    id: number;
	    name: string;
	    address: string;
	    port: number;
	    connected: boolean;
	
	    static createFrom(source: any = {}) {
	        return new ASConnection(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.id = source["id"];
	        this.name = source["name"];
	        this.address = source["address"];
	        this.port = source["port"];
	        this.connected = source["connected"];
	    }
	}

}

