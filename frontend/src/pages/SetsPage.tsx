import { Collapse, CollapseProps, List, Table, Tooltip, message } from "antd";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { GetSetsInfo } from "../../wailsjs/go/aspike/ASpike";
import { createSetKey } from "./NsSetPage";

export const SetsPage: React.FC = () => {
  const location = useLocation();

  console.log("location", location);

  // nodeId -> namespase -> set -> setInfoKeyName -> setInfoValue
  const [nodes, setNodes] = useState<
    Record<string, Record<string, Record<string, Record<string, string>>>>
  >({});

  const nodesToTableData = (
    nodes: Record<
      string,
      Record<string, Record<string, Record<string, string>>>
    >
  ) => {
    const nodeIds = new Set<string>();
    const tableData: {
      nodeName: string;
      nsName: string;
      setName: string;
      [key: string]: string;
    }[] = [];
    Object.entries(nodes).forEach(([nodeName, namespaces]) => {
      Object.entries(namespaces).forEach(([nsName, sets]) => {
        Object.entries(sets).forEach(([setName, setInfo]) => {
          nodeIds.add(nodeName);
          const id = Array.from(nodeIds).indexOf(nodeName);
          tableData.push({
            nodeId: id.toString(),
            nodeName,
            nsName,
            setName,
            ...setInfo,
          });
        });
      });
    });
    return tableData;
  };

  useEffect(() => {
    GetSetsInfo()
      .then((info) => {
        setNodes(info);
      })
      .catch((err) => {
        message.error("Error while getting sets", err);
      });
  }, []);

  console.log("nodes", nodes);

  // example data: [device_data_bytes:0 disable-eviction:false enable-index:false index_populating:false memory_data_bytes:0 ns:test objects:0 set:hits sindexes:0 stop-writes-count:0 stop-writes-size:0 tombstones:0 truncate_lut:0 truncating:false]
  return (
    <div>
      <Table
        scroll={{ x: 1500, y: "100%" }}
        size="small"
        rowKey={(rec) => createSetKey(rec.nodeName, rec.nsName, rec.setName)}
        columns={[
          {
            title: "ID",
            dataIndex: "nodeName",
            key: "id",
            render: (nodeName: string, rec, id) => (
              <Tooltip title={`${nodeName}`}>{id}</Tooltip>
            ),
            width: 40,
          },
          {
            title: "Node ID",
            dataIndex: "nodeId",
            key: "nodeId",
            width: 60,
            render: (nodeId: string, rec) => (
              <Tooltip title={`${rec.nodeName}`}>{nodeId}</Tooltip>
            ),
          },
          // {
          //   title: "Node",
          //   dataIndex: "nodeName",
          //   key: "nodeName",
          //   width: 90,
          // },
          {
            title: "Namespace",
            dataIndex: "nsName",
            key: "nsName",
          },
          {
            title: "Set",
            dataIndex: "setName",
            key: "setName",
            width: 200,
            render: (setName: string, rec) => (
              <Link
                to={`/sets/${rec.nsName}/${createSetKey(
                  rec.nodeName,
                  rec.nsName,
                  setName
                )}`}
              >
                {setName}
              </Link>
            ),
          },
          {
            title: "Objects",
            dataIndex: "objects",
            key: "objects",
          },
          {
            title: "Tombstones",
            dataIndex: "tombstones",
            key: "tombstones",
          },
          {
            dataIndex: "device_data_bytes",
            title: "Device Data Bytes",
            key: "device_data_bytes",
          },
          {
            dataIndex: "disable-eviction",
            title: "Disable Eviction",
            key: "disable-eviction",
          },
          {
            dataIndex: "enable-index",
            title: "Enable Index",
            key: "enable-index",
          },
          {
            dataIndex: "index_populating",
            title: "Index Populating",
            key: "index_populating",
          },
          {
            dataIndex: "memory_data_bytes",
            title: "Memory Data Bytes",
            key: "memory_data_bytes",
          },
          {
            dataIndex: "sindexes",
            title: "Sindexes",
            key: "sindexes",
          },
          {
            dataIndex: "stop-writes-count",
            title: "Stop Writes Count",
            key: "stop-writes-count",
          },
          {
            dataIndex: "stop-writes-size",
            title: "Stop Writes Size",
            key: "stop-writes-size",
          },
          {
            dataIndex: "truncate_lut",
            title: "Truncate Lut",
            key: "truncate_lut",
          },
          {
            dataIndex: "truncating",
            title: "Truncating",
            key: "truncating",
          },
        ]}
        dataSource={nodesToTableData(nodes)}
      />
      {/* <Collapse items={items}></Collapse> */}
    </div>
  );
};
