import { useEffect, useState } from "react";
import { GetNamespaces } from "../../wailsjs/go/aspike/ASpike";
import { List, Typography, message } from "antd";
import { Link } from "react-router-dom";

export const NamespacesPage: React.FC = () => {
  const [namespaces, setNamespaces] = useState<string[]>([]);

  useEffect(() => {
    GetNamespaces()
      .then((nss) => {
        setNamespaces(nss);
      })
      .catch((err) => {
        message.error({
          content: `Can't get namespaces list`,
        });
        console.error("Error while getting namespaces", err);
      });
  }, []);
  return (
    <div>
      <Typography.Title level={2}>Namespaces</Typography.Title>
      <List>
        {namespaces.map((ns) => (
          <List.Item key={ns}>
            <Link to={`/sets/${ns}`}>{ns}</Link>
          </List.Item>
        ))}
      </List>
    </div>
  );
};
