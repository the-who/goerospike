import { useNavigate, useParams } from "react-router-dom";
import { AppLink } from "../components/Link";
import {
  CreateConnection,
  DeleteConnection,
  GetAll,
} from "../../wailsjs/go/as_connections/AerospikeConnectionsService";
import { Connect, Disconnect } from "../../wailsjs/go/main/App";
import { useEffect, useState } from "react";
import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Table,
  message,
} from "antd";
// import { useNotification } from "../helpers/notification";

import { as_connections } from "../../wailsjs/go/models";

export const ConnectionsPage: React.FC = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [connections, setConnections] = useState<as_connections.ASConnection[]>(
    []
  );

  useEffect(() => {
    GetAll()
      .then((conns) => {
        setConnections(conns);
      })
      .catch((err) => {
        message.error({
          content: `Can't get connections list`,
        });
        console.error("Err while getting all connections", err);
      });
  }, []);

  const submit = async (values: any) => {
    await CreateConnection(values)
      .then(async () => {
        const conns = await GetAll();
        setConnections(conns);
        message.success({
          content: "Connection created",
        });
      })
      .catch((err) => {
        console.error("Error while creating connection", err);
        message.error({
          content: `Error while creating connection ${err}`,
        });
      });
  };

  const updateConnections = async () => {
    try {
      const conns = await GetAll();
      console.log(conns);
      setConnections(conns);
    } catch (err) {
      console.error("Error while getting all connections", err);
      message.error({
        content: `Error while getting all connections ${err}`,
      });
    }
  };

  return (
    <div>
      <Form
        onFinish={submit}
        form={form}
        initialValues={{
          port: 3000,
          address: "127.0.0.1",
        }}
      >
        <Row gutter={[48, 48]}>
          <Col span={8}>
            <Form.Item required label="Name" name="name">
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Button
                style={{
                  borderColor: "lightgreen",
                }}
                htmlType="submit"
              >
                Add connection
              </Button>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[48, 48]}>
          <Col span={8}>
            <Form.Item label="Address" name="address">
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Port" name="port">
              <InputNumber />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Table
        dataSource={connections}
        rowKey={"name"}
        columns={[
          { title: "ID", dataIndex: "id", key: "ID" },
          {
            title: "Name",
            dataIndex: "name",
            key: "Name",
          },
          {
            title: "Address",
            dataIndex: "address",
            key: "Address",
          },
          {
            title: "Port",
            dataIndex: "port",
            key: "Port",
          },
          {
            title: "Actions",
            // dataIndex: "Actions",
            key: "Actions",
            render: (text: any, record: as_connections.ASConnection) => (
              <Row gutter={48}>
                <Col>
                  <Button
                    type="primary"
                    danger
                    color="red"
                    onClick={async () => {
                      await DeleteConnection(record.id)
                        .then(async () => {
                          await updateConnections();
                        })
                        .catch((err) => {
                          console.error("Error while deleting connection", err);
                          message.error({
                            content: `Error while deleting connection ${err}`,
                          });
                        });
                    }}
                  >
                    Delete
                  </Button>
                </Col>
                <Col hidden={record.connected}>
                  <Button
                    type="primary"
                    onClick={() => {
                      Connect(record.id)
                        .then(() => {
                          message.success(
                            `Connected to the server ${record.name}`
                          );
                          navigate("/");
                        })
                        .catch((err) => {
                          message.error(
                            "Can not connect to the server. Please check the connection details."
                          );
                          console.error(
                            "Error while connecting to the server",
                            err
                          );
                        });
                    }}
                  >
                    Connect
                  </Button>
                </Col>
                <Col hidden={!record.connected}>
                  <Button
                    danger
                    onClick={() => {
                      Disconnect(record.id)
                        .then((res) => {
                          console.log(record);
                          message.success(
                            `Disconnected from the server ${record.name}`
                          );
                          updateConnections();
                        })
                        .catch((err) => {
                          message.error(
                            "Can not disconnect from the server. Please check the connection details."
                          );
                        });
                    }}
                  >
                    Disconnect
                  </Button>
                </Col>
              </Row>
            ),
          },
        ]}
      />
    </div>
  );
};
