import { Button, Col, Row, Tag, Tooltip } from "antd";
import { IsConnected } from "../../wailsjs/go/aspike/ASpike";
import { useEffect, useState } from "react";
import { GetCurrentConnection } from "../../wailsjs/go/as_connections/AerospikeConnectionsService";
import { useNavigate } from "react-router-dom";

export const StatusPage: React.FC = () => {
  const navigate = useNavigate();
  const [connected, setConnected] = useState<boolean>(false);
  const [currentConnection, setCurrentConnection] = useState<string>("");

  useEffect(() => {
    IsConnected()
      .then(async (connected) => {
        setConnected(connected);
        if (connected) {
          const conn = await GetCurrentConnection();
          setCurrentConnection(`${conn.address}:${conn.port}`);
        }
      })
      .catch((err) => {
        console.error("Error while checking if connected", err);
      });
  }, []);

  return (
    <div>
      <Row>
        <Col>
          <Tooltip title={`Connected to ${currentConnection}`}>
            <Tag color={connected ? "green" : "red"}>
              {connected ? "Connected" : "Disconnected"}
            </Tag>
          </Tooltip>
        </Col>
        <Col hidden={connected}>
          <Button
            type="primary"
            onClick={() => {
              navigate("/");
            }}
          >
            Go To Connections
          </Button>
        </Col>
      </Row>
    </div>
  );
};
