import { List, message } from "antd";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { GetSetsInfo } from "../../wailsjs/go/aspike/ASpike";

export const createSetKey = (nodeName: string, ns: string, set: string) =>
  `${nodeName}:${ns}:${set}`;

export const getKeyParts = (key: string) =>
  key.split(":").reduce(
    (acc, part, id) => {
      if (id === 0) {
        acc["nodeName"] = part;
      }
      if (id === 1) {
        acc["ns"] = part;
      }
      if (id === 2) {
        acc["set"] = part;
      }

      return acc;
    },
    { nodeName: "", ns: "", set: "" }
  );

const setsInfoToSetArray = (setsInfo: {
  [key: string]: {
    [key: string]: {
      [key: string]: {
        [key: string]: string;
      };
    };
  };
}) => {
  const setKeys: string[] = [];
  Object.entries(setsInfo).forEach(([nodeName, namespaces]) => {
    Object.entries(namespaces).forEach(([nsName, sets]) => {
      const setNames = Object.keys(sets);
      setKeys.push(
        ...setNames.map((setName) => createSetKey(nodeName, nsName, setName))
      );
    });
  });
  return setKeys;
};

export const NsSetPage: React.FC = () => {
  const location = useLocation();

  const [sets, setSets] = useState<string[]>([]);

  const nsName = location.pathname.split("/")[2];
  useEffect(() => {
    GetSetsInfo()
      .then((setsInfo) => {
        console.log("setsInfo", setsInfo);
        setSets(setsInfoToSetArray(setsInfo));
      })
      .catch((err) => {
        message.error("Error while getting sets", err);
      });
  }, []);

  return (
    <div>
      <List>
        {sets.map((set) => (
          <List.Item key={set}>
            <Link to={`/sets/${nsName}/${set}`}>{set}</Link>
          </List.Item>
        ))}
      </List>
    </div>
  );
};
