import { useLocation, useParams } from "react-router-dom";
import { AppLink } from "../components/Link";
import { GetSetsInfo, ScanPaginated } from "../../wailsjs/go/aspike/ASpike";
import { useEffect, useState } from "react";
import { getKeyParts, createSetKey } from "./NsSetPage";
import {
  Button,
  Col,
  Divider,
  Row,
  Table,
  TableColumnProps,
  Tooltip,
  Typography,
  message,
} from "antd";

export const SetView: React.FC = () => {
  const { pathname } = useLocation();

  const namespace = pathname.split("/")[2];
  const setKey = pathname.split("/")[3];
  const parts = getKeyParts(setKey);

  const limit = 2;

  const [currentPage, setCurrentPage] = useState<number>(1);

  const [totalObjects, setTotalObjects] = useState<number>(0);

  // const [recs, setRecs] = useState<any>({});
  const [paginated, setPaginated] = useState<
    {
      [key: string]: any;
    }[]
  >([]);
  const [cursor, setCursor] = useState<number[]>([]);
  const [allCursors, setAllCursors] = useState<Record<string, number[]>>({});

  if (!namespace || !setKey) {
    return <div>Invalid namespace or set key</div>;
  }

  useEffect(() => {
    const f = async () => {
      try {
        const setInfo = await GetSetsInfo();
        let objectsCount = 0;
        Object.keys(setInfo).forEach((k) => {
          const objects = setInfo[k][parts.ns][parts.set].objects;
          objectsCount += +objects;
        });
        setTotalObjects(objectsCount);
        const p = await ScanPaginated(
          parts.ns,
          parts.set,
          0,
          limit,
          cursor || []
        );
        if (p) {
          console.log("paginated result", {
            ...p,
            cursor: p.Cursor.slice(-20),
          });
          setPaginated(p.Records);
          setCursor(p.Cursor);
          setAllCursors({ 1: p.Cursor });
          setCurrentPage(1);
        }
      } catch (err) {
        console.log("error1", err);
        message.error("Error while getting paginated records");
      }
    };
    f();
  }, []);

  const rendomObjectToTableData = (obj: any) => {
    if (!obj) {
      return [];
    }
    const tableData: TableColumnProps<any>[] = [];
    Object.entries(obj).forEach(([key, value], i) => {
      tableData.push({
        title: key,
        dataIndex: key,
        key: crypto.getRandomValues(new Uint32Array(10))[0].toString(),
        // key: `${JSON.stringify(value).slice(0, 20)}_${key}_${i}`,
        render: (text: string, rec: any, id) => {
          if (
            Array.isArray(text) &&
            text.every(
              (t) =>
                t.Key &&
                t.Value &&
                typeof t.Key === "string" &&
                typeof t.Value === "string"
            )
          ) {
            return text.map((t, i) => {
              return (
                <Row key={i}>
                  {i === 0 ? <Divider /> : <></>}
                  <Typography.Text>{t.Key}</Typography.Text>:&nbsp;
                  <Typography.Text>
                    {t.Value.length > 10 ? (
                      <Tooltip title={t.Value}>
                        {t.Value.slice(0, 5) + "..." + t.Value.slice(-5)}
                      </Tooltip>
                    ) : (
                      t.Value
                    )}
                  </Typography.Text>
                  <Divider />
                </Row>
              );
            });
          }
          if (typeof text === "object") {
            return JSON.stringify(text);
          }
          return text;
        },
      });
    });
    return tableData;
  };
  return (
    <div style={{ height: "100%" }}>
      <AppLink to="/">Back</AppLink>
      <Col>
        <h1>{`${parts.ns} / ${parts.set}`}</h1>
        <Button
          disabled={currentPage === 1}
          onClick={() => {
            // console.log("prevCursor", prevCursor);
            // if (newPrevCursors.length === 1) {
            //   setPrevCursors([prevCursor]);
            //   console.log(
            //     "first page reached",
            //     newPrevCursors.length,
            //     prevCursor
            //   );
            //   return;
            // }
            // setAllCursors(prevCursors.slice(0, prevCursors.length - 1));
            ScanPaginated(
              parts.ns,
              parts.set,
              0,
              limit,
              allCursors[currentPage - 2]
            )
              .then((p) => {
                if (p) {
                  console.log("paginated result", p);
                  setPaginated(p.Records);
                  setCursor(allCursors[currentPage - 1]);
                  setCurrentPage(currentPage - 1);
                }
              })
              .catch((err) => {
                console.log("error2", err);
                message.error("Error while getting paginated records");
              });
          }}
        >
          Prev
        </Button>
        <Button
          disabled={currentPage * limit + limit > totalObjects}
          onClick={() => {
            // if (limit + prevCursors.length * limit > totalObjects) {
            //   console.log(
            //     "no more records",
            //     totalObjects,
            //     limit,
            //     prevCursors.length * limit
            //   );
            //   return;
            // }
            // setPrevCursors([...prevCursors, cursor]);
            ScanPaginated(parts.ns, parts.set, 0, limit, cursor || [])
              .then((p) => {
                if (p) {
                  console.log("paginated result", p);
                  setPaginated(p.Records);
                  setCursor(p.Cursor);
                  const nextPage = currentPage + 1;
                  const nextAllCursors: Record<string, number[]> = {
                    ...allCursors,
                    [nextPage]: p.Cursor,
                  };
                  setAllCursors(nextAllCursors);
                  setCurrentPage(nextPage);
                  console.log(
                    "allCursors",
                    JSON.stringify(
                      Object.entries(nextAllCursors).map(([k, v]) => {
                        return { [k]: v.slice(-20) };
                      }),
                      null,
                      2
                    )
                  );
                }
              })
              .catch((err) => {
                console.log("error3", err);
                message.error("Error while getting paginated records");
              });
          }}
        >
          Next
        </Button>

        <Typography.Text>Total objects: {totalObjects}</Typography.Text>
        <Typography.Text>Current page: {currentPage}</Typography.Text>
        <div id="result2" className="result">
          {paginated && paginated.length !== 0 ? (
            <Table
              dataSource={paginated}
              columns={rendomObjectToTableData(paginated[0])}
              scroll={{ x: 1500, y: "100%" }}
              size="small"
              // rowKey={(rec, i) => {
              //   const id = (i || 0) + 1;
              //   return `${JSON.stringify(rec).slice(0, 20)}${id}`;
              // }}
              rowKey={(rec) => {
                return crypto
                  .getRandomValues(new Uint32Array(10))[0]
                  .toString();
              }}
            />
          ) : (
            <Typography.Text>
              No records found for {parts.ns} / {parts.set}
            </Typography.Text>
          )}
        </div>
      </Col>
    </div>
  );
};
