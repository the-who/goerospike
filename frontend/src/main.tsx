import React from "react";
import { createRoot } from "react-dom/client";
import "./style.css";
import App from "./App";
import { HashRouter, Route, Routes } from "react-router-dom";
import { SetView } from "./pages/SetView";

const container = document.getElementById("root");

const root = createRoot(container!);

root.render(
  <React.StrictMode>
    <HashRouter basename={"/"}>
      <App />
    </HashRouter>
  </React.StrictMode>
);
