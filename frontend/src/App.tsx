import React, { useEffect, useState } from "react";
// import logo from "./assets/images/logo-universal.png";
import "./App.css";
import { Layout, Menu, MenuProps, theme } from "antd";
import { Link, Route, Routes, useLocation, useRoutes } from "react-router-dom";
import {
  AppstoreOutlined,
  CloudServerOutlined,
  DotChartOutlined,
  OrderedListOutlined,
} from "@ant-design/icons";
import { ConnectionsPage } from "./pages/ConnectionsPage";
import { StatusPage } from "./pages/StatusPage";
import { NamespacesPage } from "./pages/NamespacesPage";
import { SetsPage } from "./pages/SetsPage";
import { NsSetPage } from "./pages/NsSetPage";
import { SetView } from "./pages/SetView";

const { Header, Content, Sider } = Layout;

const menuItems: MenuProps["items"] = [
  { path: "/", icon: CloudServerOutlined, label: "Connections" },
  { path: "/namespaces", icon: OrderedListOutlined, label: "Namespaces" },
  { path: "/sets", icon: DotChartOutlined, label: "Sets" },
  { path: "/status", icon: AppstoreOutlined, label: "App Status" },
].map((item) => ({
  key: String(item.path),
  icon: React.createElement(item.icon),
  label: (
    <Link className="nav-link" to={item.path}>
      {item.label}
    </Link>
  ),
}));

function App() {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  const location = useLocation();

  console.log("location", location);

  return (
    <Layout hasSider>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
      >
        {/* <img src={logo} id="logo" alt="logo" width={"50px"} height={"50px"} /> */}
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[location.pathname]}
          selectedKeys={[location.pathname]}
          items={menuItems}
        />
      </Sider>
      <Layout style={{ marginLeft: 200, padding: 10 }}>
        <Header style={{ padding: 0, background: colorBgContainer }} />
        <Content
          style={{
            overflowX: "hidden",
          }}
        >
          <Routes>
            <Route path="/" element={<ConnectionsPage />} />
            <Route path="/namespaces" element={<NamespacesPage />} />
            <Route path="/sets" element={<SetsPage />} />
            <Route path="/sets/:ns" element={<NsSetPage />} />
            <Route path="/sets/:ns/:set" element={<SetView />} />
            <Route path="/status" element={<StatusPage />} />
          </Routes>
        </Content>
      </Layout>
    </Layout>
  );
}

export default App;
