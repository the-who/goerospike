import { Link, LinkProps } from "react-router-dom";

export const AppLink = (props: LinkProps) => {
  return (
    <Link {...props} className="link" color="white">
      {props.children}
    </Link>
  );
};
