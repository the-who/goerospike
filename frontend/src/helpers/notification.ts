import { notification } from "antd";

type NotificationType = "success" | "info" | "warning" | "error";

export const useNotification = () => {
  const [api, contextHolder] = notification.useNotification();

  return api;
};
