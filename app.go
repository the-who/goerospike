package main

import (
	"context"
	"fmt"
	"log"

	// "errors"

	// "log"
	// "database/sql"
	"goerospike/as_connections"
	aspike "goerospike/aspike"

	_ "github.com/mattn/go-sqlite3"
)

// App struct
type App struct {
	ctx context.Context
	repo as_connections.ASConnectionsRepository
	aspike *aspike.ASpike
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}

func (a *App) SetRepo(repo as_connections.ASConnectionsRepository) {
	a.repo = repo
}

func (a *App) SetASpike(as *aspike.ASpike) {
	a.aspike = as
}

func (a *App) Connect(connectionId int64) error {
	a.aspike.Close()

	a.repo.DisconnectAll(a.ctx)

	conn, err := a.repo.GetById(a.ctx, connectionId)
	if err != nil {
		log.Println(err, connectionId)
		return err
	}


	fmt.Printf("Connecting to %s:%d\n", conn.Address, conn.Port)

	err = a.aspike.Connect(conn.Address, int(conn.Port))
	if err != nil {
		fmt.Printf("Error occured %v", err)
		return err
	}

	a.repo.Update(a.ctx, connectionId, as_connections.ASConnection{
		ID: conn.ID,
		Name: conn.Name,
		Address: conn.Address,
		Port: conn.Port,
		Connected: true,
	})

	return nil
}

func (a *App) Disconnect(connectionId int64) {
	a.aspike.Close()

	a.repo.DisconnectAll(a.ctx)
}
