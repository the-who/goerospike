package aspike

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	aero "github.com/aerospike/aerospike-client-go/v7"
)

type ASpike struct {
	client *aero.Client
}

func NewASpike() *ASpike {
	return &ASpike{}
}

func (aspike *ASpike) Connect(address string, port int) error {
	clientPolicy := aero.NewClientPolicy()
	clientPolicy.Timeout = 500 * time.Millisecond

	client, aeroerr := aero.NewClientWithPolicyAndHost(clientPolicy, &aero.Host{Name: address, Port: port})
	if aeroerr != nil {
		log.Printf("Error connecting to Aerospike cluster: %v", aeroerr)
		return aeroerr
	}

	aspike.client = client
	return nil
}

func (aspike *ASpike) Close() {
	if aspike.client != nil && aspike.client.IsConnected() {
		aspike.client.Close()
	}
}


func (aspike *ASpike) GetNamespaces() []string {
	nodes := aspike.client.GetNodes()

	infop := aero.NewInfoPolicy()

	result := []string{}

	for _, n := range nodes {
		info, aeroerr := n.RequestInfo(infop, "namespaces")
		if aeroerr != nil {
			log.Fatalf("Error requesting info from Aerospike cluster: %v", aeroerr)
		}
		// log.Printf("Aerospike cluster info: %v", info)
		for _, v := range info {
			// log.Printf("Aerospike cluster info: %v \n %v", k, v)
			result = append(result, v)
		}
	}

	return result
}


func parseSetInfo(info string) map[string]string {
	infoMap := map[string]string{}
	infoParts := strings.Split(info, ":")
	for _, part := range infoParts {
		parts := strings.Split(part, "=")
		if len(parts) == 2 {
			infoMap[parts[0]] = parts[1]
		}
	}
	return infoMap
}

type SetInfo = map[string]map[string]map[string]map[string]string

func (aspike *ASpike) GetSetsInfo() (SetInfo, error) {
	nodes := aspike.client.GetNodes()
	// fmt.Printf("nodes: %v \n", nodes)

	infop := aero.NewInfoPolicy()

	result := SetInfo{}

	for _, n := range nodes {
		info, aeroerr := n.RequestInfo(infop, "sets")
		if aeroerr != nil {
			return nil, fmt.Errorf("error requesting info from Aerospike cluster: %v", aeroerr)
		}

		nodeName := n.GetName()
		// fmt.Printf("nodeName: %v \n", nodeName)
		for _, v := range info {
			setsInfo := strings.Split(v, ";")
			// setsMap := map[string]map[string]string{}
			for _, info := range setsInfo {
				setInfoParts := strings.Split(info, ":")
				if len(setInfoParts) < 2 {
					continue
				}
				setInfo := parseSetInfo(info)
				nsArr := strings.Split(setInfoParts[0], "=")
				ns := nsArr[1]
				setNameArr := strings.Split(setInfoParts[1], "=")
				setName := setNameArr[1]
				if _, ok := result[nodeName]; !ok {
					result[nodeName] = map[string]map[string]map[string]string{}
				}
				if _, ok := result[nodeName][ns]; !ok {
					result[nodeName][ns] = map[string]map[string]string{}
				}
				result[nodeName][ns][setName] = setInfo
			}
		}
	}
	// fmt.Printf("SetsInfo: %v \n", result)
	return result, nil
}

// get all records from set
// func (aspike *ASpike) GetRecords(ns string, s string) []map[string]interface{} {
// 	spol := aero.NewScanPolicy()

// 	recres, err := aspike.client.ScanAll(spol, ns, s)

// 	if err != nil {
// 		log.Fatalf("Error scanning set: %v", err)
// 		log.Fatalln(err.Error())
// 	}

// 	log.Printf("Scan results: %v", recres)

// 	result := []map[string]interface{}{}

// 	for rec := range recres.Results() {
// 		if rec.Err != nil {
// 			// if there was an error, handle it if needed
// 			// Scans are retried in Aerospike servers v5+
// 			log.Println(err)
// 			continue
// 		}
// 		// fmt.Printf("Record: %v \n", rec.Record)
// 		// get type of record
// 		// fmt.Printf("Record type: %v \n", reflect.TypeOf(rec.Record.Bins))
// 		result = append(result, rec.Record.Bins)
// 	}

// 	return result
// }

type Res struct {
	Records []map[string]interface{}
	Cursor []byte
}

// inspired by: https://pkg.go.dev/github.com/aerospike/aerospike-client-go/v7@v7.1.0#example-PartitionFilter.EncodeCursor
func (aspike *ASpike) ScanPaginated(ns string, s string, page int, pageSize int, crsr []byte) (*Res, error) {
	keyCount := pageSize
	// Set up the scan policy
	spolicy := aero.NewScanPolicy()
	spolicy.MaxRecords = int64(pageSize)
	spolicy.IncludeBinData = true

	received := 0

	result := []map[string]interface{}{}

	var cursor []byte
	if len(crsr) > 0 {
		cursor = crsr
	}
	iterations := 0
	for received < keyCount {
		if (iterations > pageSize + 10 && received > 0) {
			break;
		}
		if iterations > pageSize + 10 {
			fmt.Printf("Too many iterations in paginated scan: %v", iterations)
			fmt.Printf(("Cursor: %v"), cursor[len(cursor)-20:])
			return nil, fmt.Errorf("too many iterations in paginated scan: %v", iterations)
		}
		pf := aero.NewPartitionFilterAll()

		if len(cursor) > 0 {
			err := pf.DecodeCursor(cursor)
			if err != nil {
				return nil, fmt.Errorf("error while decoding cursor: %v", err)
			}
			fmt.Printf("\nBegin: %v | Count: %v | Received: %v | KeyCount: %v | %v \n\n", pf.Begin, pf.Count, received, keyCount, cursor[len(cursor)-20:])
			// if pageSize > 0 {
			// 	pf.Count = pageSize
			// 	pf.Begin = pf.Begin + pageSize
			// }
			// if pageSize < 0 {
			// 	if (pf.Begin - pageSize) < 0 {
			// 		pf.Begin = 0
			// 	} else {
			// 		pf.Begin = pf.Begin - pageSize
			// 	}
			// }
		}

		recordset, err := aspike.client.ScanPartitions(spolicy, pf, ns, s)
		if err != nil {
			fmt.Printf("Error scanning partitions: %v", err)
			return nil, fmt.Errorf("error while scanning partitions: %v", err)
		}

		counter := 0
		for v := range recordset.Results() {
			if v.Err != nil {
				if v.Err.Matches(-3)  {
					// no more records
					break
				}
				fmt.Printf("Error scanning recordset: %v", v.Err.Error())
				return nil, fmt.Errorf("error scanning recordset: %v", v.Err.Error())
			} else {
				// process record here
				fmt.Printf("\nBins: %v", v)
				r := v.Record.Bins
				r["KEY"] = v.Record.Key.String()
				result = append(result, r)
			}
			counter++
			received++
		}

		if counter > pageSize {
			return nil, fmt.Errorf("more records received than requested")
		}

		cursor, err = pf.EncodeCursor()
		if err != nil {
			fmt.Printf("error while encoding cursor: %v", err)
			return nil, fmt.Errorf("more records received than requested")
		}
		iterations++
	}

	fmt.Println("\nreceived", received)
	b, e := json.Marshal(result)
	if e != nil {
		fmt.Printf("cant marshal result as json: %v", e)
	}
	fmt.Printf("\nresult %v", string(b))
	// fmt.Println("buf", buf)

	return &Res{Records: result, Cursor: cursor}, nil
}

func (aspike *ASpike) ScanParallel(ns string, s string, page int, pageSize int) ([]map[string]interface{}, error) {
	log.Printf("Scan parallel: namespace=" + ns + " set=" + s)
	recordCount := 0
	begin := time.Now()
	policy := aero.NewScanPolicy()
	policy.MaxRecords = int64(pageSize)

	pf := aero.NewPartitionFilterAll()

	receivedRecords := 1
	for receivedRecords > 0 {
		receivedRecords = 0

		log.Println("Scanning Page:", recordCount/int(policy.MaxRecords))
		recordset, err := aspike.client.ScanPartitions(policy, pf, ns, s)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		for rec := range recordset.Results() {
			if rec.Err != nil {
				// if there was an error, handle it if needed
				// Scans are retried in Aerospike servers v5+
				log.Println(err)
				continue
			}

			recordCount++
			receivedRecords++
		}
	}

	log.Println("Total records returned: ", recordCount)
	log.Println("Elapsed time: ", time.Since(begin), " seconds")

	return nil, nil
}

func (aspike *ASpike) IsConnected() bool {
	return aspike.client != nil && aspike.client.IsConnected()
}
