package main

import (
	"embed"
	"fmt"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/logger"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"

	"context"
	"log"

	"database/sql"
	"goerospike/as_connections"
	aspike "goerospike/aspike"

	_ "github.com/mattn/go-sqlite3"
)

//go:embed all:frontend/dist
var assets embed.FS

const fileName = "as_connections.db"

func main() {
	// Create an instance of the app structure
	app := NewApp()

	as := aspike.NewASpike()

	db, dberr := sql.Open("sqlite3", fileName)
	if dberr != nil {
			log.Fatal(dberr)
	}

	connectionRepo := as_connections.NewSQLiteRepository(db)

	ctx := context.Background()

	if err := connectionRepo.Migrate(ctx); err != nil {
		log.Fatal(err)
	}

	currentConnection, repoerr := connectionRepo.GetCurrentConnection(ctx)
	if repoerr != nil {
		log.Println(repoerr)
	}
	if currentConnection != nil {
		err := as.Connect(currentConnection.Address, int(currentConnection.Port))
		if err != nil {
			fmt.Errorf("can not connect to aerospike %v", err)
			connectionRepo.DisconnectAll(ctx)
		} else {
			fmt.Printf("Connected to %s:%d\n", currentConnection.Address, currentConnection.Port)

		}
	}

	asConnectionsService := as_connections.NewAerospikeConnectionsService(connectionRepo, &ctx)

	app.SetRepo(connectionRepo)
	app.SetASpike(as)

	// Create application with options
	err := wails.Run(&options.App{
		Title:  "GOerospike (Aerospike client)",
		Width:  1366,
		Height: 1024,
		AssetServer: &assetserver.Options{
			Assets: assets,
		},
		LogLevel: logger.DEBUG,
		BackgroundColour: &options.RGBA{R: 27, G: 38, B: 54, A: 1},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
			as,
			asConnectionsService,
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}
}
