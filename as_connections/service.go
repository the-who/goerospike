package as_connections

import (
	"context"
	// "log"
)

type AerospikeConnectionsService struct {
	repo ASConnectionsRepository
	ctx *context.Context
}

func NewAerospikeConnectionsService(r ASConnectionsRepository, c *context.Context) *AerospikeConnectionsService {
	return &AerospikeConnectionsService{
		repo: r,
		ctx: c,
	}
}

func (s *AerospikeConnectionsService) CreateConnection(c ASConnection) (*ASConnection, error) {
	conn, err := s.repo.Create(*s.ctx, c)

	if err != nil {
		return nil, err
	}

	return conn, err
}

func (s *AerospikeConnectionsService) GetAll() ([]ASConnection, error) {
	conns, err := s.repo.All(*s.ctx)

	if (err != nil) {
		return nil, err
	}

	return conns, err
}

func (s *AerospikeConnectionsService) GetByName(name string) (*ASConnection, error) {
	conn, err := s.repo.GetByName(*s.ctx, name)

	if (err != nil) {
		return nil, err
	}

	return conn, err
}

func (s *AerospikeConnectionsService) UpdateConnection(id int64, c ASConnection) (*ASConnection, error) {
	conn, err := s.repo.Update(*s.ctx, id, c)

	if (err != nil) {
		return nil, err
	}

	return conn, err
}

func (s *AerospikeConnectionsService) GetCurrentConnection() (*ASConnection, error) {
	conn, err := s.repo.GetCurrentConnection(*s.ctx)

	if (err != nil) {
		return nil, err
	}

	return conn, nil
}

func (s *AerospikeConnectionsService) DeleteConnection(id int64) error {
	err := s.repo.Delete(*s.ctx, id)

	if (err != nil) {
		return err
	}

	return err
}