package as_connections

import (
	"context"
	"database/sql"
	"errors"

	"github.com/mattn/go-sqlite3"
)

type SQLiteRepository struct {
	db *sql.DB
}

func NewSQLiteRepository(db *sql.DB) *SQLiteRepository {
	return &SQLiteRepository{
		db: db,
	}
}

func (r *SQLiteRepository) Migrate(ctx context.Context) error {
	query := `
		CREATE TABLE IF NOT EXISTS as_connections(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name TEXT NOT NULL UNIQUE,
			address TEXT NOT NULL,
			port INTEGER NOT NULL,
			connected BOOLEAN DEFAULT FALSE
		);
	`

	_, err := r.db.ExecContext(ctx, query)
	return err
}

func (r *SQLiteRepository) Create(ctx context.Context, connection ASConnection) (*ASConnection, error) {
	var id int64
	err := r.db.QueryRowContext(ctx, `
		INSERT INTO as_connections(name, address, port) values($1, $2, $3) RETURNING id;
	`, connection.Name, connection.Address, connection.Port).Scan(&id)

	if err != nil {
		var sqliteError sqlite3.Error
		if (errors.As(err, &sqliteError)) {
			if (errors.Is(sqliteError.ExtendedCode, sqlite3.ErrConstraintUnique)) {
				return nil, ErrDuplicate
			}
		}
		return nil, err
	}

	connection.ID = id

	return &connection, nil
}

func (r *SQLiteRepository) All(ctx context.Context) ([]ASConnection, error) {
	rows, err := r.db.QueryContext(ctx, `SELECT * FROM as_connections`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var connections []ASConnection
	for rows.Next() {
		var conn ASConnection
		if err := rows.Scan(&conn.ID, &conn.Name, &conn.Address, &conn.Port, &conn.Connected); err != nil {
			return nil, err
		}
		connections = append(connections, conn)
	}

	return connections, nil
}

func (r *SQLiteRepository) GetById(ctx context.Context, id int64) (*ASConnection, error) {
	row := r.db.QueryRowContext(ctx, `SELECT * FROM as_connections WHERE id = $1`, id)

	var conn ASConnection
	if err := row.Scan(&conn.ID, &conn.Name, &conn.Address, &conn.Port, &conn.Connected); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}

	return &conn, nil
}

func (r *SQLiteRepository) DisconnectAll(ctx context.Context) error {
	_, err := r.db.ExecContext(ctx, `UPDATE as_connections SET connected = FALSE`)
	return err
}

func (r *SQLiteRepository) GetCurrentConnection(ctx context.Context) (*ASConnection, error) {
	row := r.db.QueryRowContext(ctx, `SELECT * FROM as_connections WHERE connected = TRUE`)

	var conn ASConnection
	if err := row.Scan(&conn.ID, &conn.Name, &conn.Address, &conn.Port, &conn.Connected); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}

	return &conn, nil
}

func (r *SQLiteRepository) GetByName(ctx context.Context, name string) (*ASConnection, error) {
	row := r.db.QueryRowContext(ctx, `SELECT * FROM as_connections WHERE name = $1`, name)

	var conn ASConnection
	if err := row.Scan(&conn.ID, &conn.Name, &conn.Address, &conn.Port, &conn.Connected); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}

	return &conn, nil
}

func (r *SQLiteRepository) Update(ctx context.Context, id int64, updated ASConnection) (*ASConnection, error) {
	if id == 0 {
		return nil, errors.New("invalid updated ID")
	}

	res, err := r.db.ExecContext(ctx, `UPDATE as_connections SET name = $1, address = $2, port = $3, connected = $4 WHERE id = $5`, updated.Name, updated.Address, updated.Port, updated.Connected, id)
	if err != nil {
		var sqliteError sqlite3.Error
		if (errors.As(err, &sqliteError)) {
			if (errors.Is(sqliteError.ExtendedCode, sqlite3.ErrConstraintUnique)) {
				return nil, ErrDuplicate
			}
		}
		return nil, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}

	if rowsAffected == 0 {
		return nil, ErrUpdateFailed
	}

	return &updated, nil
}

// func (r *SQLiteRepository) UpdateMany(ctx context.Context, updated []ASConnection) error {
// 	tx, err := r.db.BeginTx(ctx, nil)
// 	if err != nil {
// 		return err
// 	}

// 	for _, conn := range updated {
// 		_, err := tx.ExecContext(ctx, `UPDATE as_connections SET name = $1, address = $2, port = $3, connected = $4 WHERE id = $5`, conn.Name, conn.Address, conn.Port, conn.Connected, conn.ID)
// 		if err != nil {
// 			tx.Rollback()
// 			return err
// 		}
// 	}

// 	return tx.Commit()
// }

func (r *SQLiteRepository) Delete(ctx context.Context, id int64) error {
	res, err := r.db.ExecContext(ctx, `DELETE FROM as_connections WHERE id = $1`, id)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDeleteFailed
	}

	return err
}
