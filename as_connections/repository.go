package as_connections

import (
	"context"
	"errors"
)

var (
	ErrDuplicate    = errors.New("connection already exists")
	ErrNotExist     = errors.New("connection does not exist")
	ErrUpdateFailed = errors.New("connection update failed")
	ErrDeleteFailed = errors.New("connection delete failed")
)

type ASConnectionsRepository interface {
	Migrate(ctx context.Context) error
	Create(ctx context.Context, connection ASConnection) (*ASConnection, error)
	All(ctx context.Context) ([]ASConnection, error)
	GetByName(ctx context.Context, name string) (*ASConnection, error)
	GetById(ctx context.Context, id int64) (*ASConnection, error)
	GetCurrentConnection(ctx context.Context) (*ASConnection, error)
	DisconnectAll(ctx context.Context) error
	Update(ctx context.Context, id int64, updated ASConnection) (*ASConnection, error)
	Delete(ctx context.Context, id int64) error
}