package as_connections

type ASConnection struct {
	ID int64 `json:"id"`
	Name string `json:"name"`
	Address string `json:"address"`
	Port uint16 `json:"port"`
	Connected bool `json:"connected"`
}
